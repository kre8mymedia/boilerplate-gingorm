#!/bin/sh

if [ -z "$1" ]
then
      echo "ERROR: First argument for ENV_FILE_NAME is empty"
      exit
fi

### TEST PIPE
### Set Environment Variables
set -a # automatically export all variables
source $1
set +a

go clean -testcache
go test ./services
