package users

import (
	"fmt"

	"github.com/kre8mymedia/gingorm/auth"
	"github.com/kre8mymedia/gingorm/models"
)

func AuthUser(
	authHeader string,
) (models.User) {
	token, err := auth.ExtractClaims(authHeader)
	if err != false {
		fmt.Print(err)
	}

	var id string = fmt.Sprintf("%v", token["id"])
	var user models.User
	if err := models.DB.Where("id = ?", id).First(&user).Error; err != nil {
	  panic(err)
	}
	return user
}