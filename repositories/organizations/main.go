package organizations

import (

	"github.com/kre8mymedia/gingorm/models"

	"github.com/jinzhu/gorm"
)

func All() ([]models.Organization) {
	var organizations []models.Organization
  	models.DB.Find(&organizations)
	return organizations
}

func Create(orgInput models.Organization) (*gorm.DB) {
	org := models.DB.Create(&orgInput)
	return org
}


func FetchWithUsers(
	id string, 
	fields []string,
) (models.Organization) {
	var organization models.Organization
	models.DB.Preload("Users", func(db *gorm.DB) *gorm.DB {
		return db.Select(fields)
	}).Where("id = ?", id).First(&organization)
	return organization
}