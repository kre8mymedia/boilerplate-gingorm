package models

import (
	"time"
	"github.com/aidarkhanov/nanoid"
	"github.com/jinzhu/gorm"
)

type Organization struct {
	ID            string     `json:"id" gorm:"primary_key"`
	CreatedAt     time.Time  `json:"createdAt"`
	UpdatedAt     time.Time  `json:"-"`
	DeletedAt     *time.Time `json:"-"`
	PrimaryUserID string     `json:"primaryUserId" gorm:"unique"`
	Name          string     `json:"name"`
	Slug          string     `json:"slug" gorm:"unique"`
	Users         []User     `json:"users,omitempty"`
}

type ListOfOrganizations struct {
	Organizations []OrganizationNoRelations `json:"organizations"`
}

type OrganizationWithRelations struct {
	ID            string           `json:"id" gorm:"primary_key"`
	CreatedAt     time.Time        `json:"createdAt"`
	UpdatedAt     time.Time        `json:"-"`
	DeletedAt     *time.Time       `json:"-"`
	PrimaryUserID string           `json:"primaryUserId" gorm:"unique"`
	Name          string           `json:"name"`
	Slug          string           `json:"slug" gorm:"unique"`
	Users         []UserNoPassword `json:"users,omitempty"`
}

type OrganizationNoRelations struct {
	ID            string     `json:"id" gorm:"primary_key"`
	CreatedAt     time.Time  `json:"createdAt"`
	UpdatedAt     time.Time  `json:"-"`
	DeletedAt     *time.Time `json:"-"`
	PrimaryUserID string     `json:"primaryUserId" gorm:"unique"`
	Name          string     `json:"name"`
	Slug          string     `json:"slug" gorm:"unique"`
}

type SingleOrganizationNoRelationsResponse struct {
	Success      bool                    `json:"success"`
	Organization OrganizationNoRelations `json:"organization"`
}

type SingleOrganizationWithRelationsResponse struct {
	Success      bool                    `json:"success"`
	Organization OrganizationWithRelations `json:"organization"`
}

type CreateOrganizationInput struct {
	Name string `json:"name" binding:"required"`
	Slug string `json:"slug" binding:"required"`
}

func (o *Organization) BeforeCreate(db *gorm.DB) error {
	o.ID = nanoid.New()
	return nil
}
