package models

import (
	"time"

	"github.com/aidarkhanov/nanoid"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID             string       `json:"id" gorm:"primary_key"`
	Name           string       `json:"name" binding:"required"`
	Username       string       `json:"username" binding:"required" gorm:"unique"`
	Email          string       `json:"email" binding:"required" gorm:"unique"`
	Password       string       `json:"password,omitempty" binding:"required,min=8"`
	OrganizationID string       `json:"-"`
	Organization   Organization `json:"-"`
	CreatedAt      time.Time    `json:"-"`
	UpdatedAt      time.Time    `json:"-"`
	DeletedAt      *time.Time   `json:"-"`
}

type UserNoPassword struct {
	ID             string       `json:"id" gorm:"primary_key"`
	Name           string       `json:"name" binding:"required"`
	Username       string       `json:"username" binding:"required" gorm:"unique"`
	Email          string       `json:"email" binding:"required" gorm:"unique"`
	OrganizationID string       `json:"-"`
	Organization   Organization `json:"-"`
	CreatedAt      time.Time    `json:"-"`
	UpdatedAt      time.Time    `json:"-"`
	DeletedAt      *time.Time   `json:"-"`
}

type UserNoPasswordOrName struct {
	ID             string       `json:"id" gorm:"primary_key"`
	Username       string       `json:"username" binding:"required" gorm:"unique"`
	Email          string       `json:"email" binding:"required" gorm:"unique"`
	OrganizationID string       `json:"-"`
	Organization   Organization `json:"-"`
	CreatedAt      time.Time    `json:"-"`
	UpdatedAt      time.Time    `json:"-"`
	DeletedAt      *time.Time   `json:"-"`
}

type CreateUserInput struct {
	Name     string `json:"name" binding:"required"`
	Username string `json:"username" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (u *User) BeforeCreate(db *gorm.DB) error {
	u.ID = nanoid.New()
	return nil
}

func (user *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}
func (user *User) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}
