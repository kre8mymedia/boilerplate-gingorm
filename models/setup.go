package models

import (
  "os"
  "log"
  "github.com/jinzhu/gorm"
  _ "github.com/go-sql-driver/mysql"
)

var DB *gorm.DB

func ConnectDatabase() {
  DB_HOST := os.Getenv("DB_HOST")
  DB_PORT := os.Getenv("DB_PORT")
  DB_DATABASE := os.Getenv("DB_DATABASE")
  DB_USERNAME := os.Getenv("DB_USERNAME")
  DB_PASSWORD := os.Getenv("DB_PASSWORD")

  database, err := gorm.Open(
    "mysql",
    DB_USERNAME+":"+DB_PASSWORD+"@tcp("+DB_HOST+":"+DB_PORT+")/"+DB_DATABASE+"?charset=utf8&parseTime=True&loc=Local",
  )

  if err != nil {
    panic("Failed to connect to database!")
  }

  database.AutoMigrate(&Book{}, &Organization{}, &User{})
  database.Model(&Book{}).ModifyColumn("id", "varchar(30)")
  log.Println("Database Migration Completed!")

  DB = database
}

