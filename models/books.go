package models

import (
	"github.com/aidarkhanov/nanoid"
	"github.com/jinzhu/gorm"
)

type Book struct {
	ID     string `json:"id" gorm:"primary_key"`
	Title  string `json:"title"`
	Author string `json:"author"`
}

type ListOfBooks struct {
	Books []Book `json:"books"`
}

type SingleBookResponse struct {
	Books Book `json:"book"`
}

type CreateBookInput struct {
	Title  string `json:"title" binding:"required"`
	Author string `json:"author" binding:"required"`
}

type UpdateBookInput struct {
	Title  string `json:"title"`
	Author string `json:"author"`
}

func (b *Book) BeforeCreate(db *gorm.DB) error {
	b.ID = nanoid.New()
	return nil
}
