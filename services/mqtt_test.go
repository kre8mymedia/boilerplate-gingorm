package services

import (
	"os"
	"log"
	"testing"
	"github.com/kre8mymedia/gingorm/entities"
	"github.com/joho/godotenv"
)

func loadVars() {
	err := godotenv.Load("../.env.test")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	
	os.Setenv("MQTT_HOST", HOST)
	os.Setenv("MQTT_PORT", PORT)
	os.Setenv("MQTT_USERNAME", USERNAME)
	os.Setenv("MQTT_PASSWORD", MQTT_PASSWORD)
}

func TestPublish(t *testing.T){
	loadVars()
	
    published, err := Publish(entities.Topic["test"], "Successful test")

    if err != nil {
		println("ERROR")
	}
	println(published)
}

func TestThreadedPublish(t *testing.T){
	loadVars()
	
    published, err := ThreadedPublish(100, entities.Topic["test"], "Successful test")

    if err != nil {
		println("ERROR")
	}
	println(published)
}
