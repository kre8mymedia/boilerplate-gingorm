package services

import (
	"context"
	"crypto/tls"
	"os"

	"github.com/go-redis/redis/v8"
)

func client() (*redis.Client, context.Context) {
	var REDIS_HOST string = os.Getenv("REDIS_HOST")
	var REDIS_PORT string = os.Getenv("REDIS_PORT")
	var REDIS_USER string = os.Getenv("REDIS_USER")
	var REDIS_PASSWORD string = os.Getenv("REDIS_PASSWORD")
	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     REDIS_HOST + ":" + REDIS_PORT,
		Username: REDIS_USER,
		Password: REDIS_PASSWORD, // no password set
		DB:       0,              // use default DB
		TLSConfig: &tls.Config{
			MinVersion: tls.VersionTLS12,
			//Certificates: []tls.Certificate{cert}
		},
	})
	return rdb, ctx
}

func RedisGet(key string) (interface{}, error) {
	rdb, ctx := client()
	val, err := rdb.Get(ctx, key).Result()
	if err != nil {
		panic(err)
	}
	return val, err
}

func RedisSet(
	key string,
	value interface{},
) bool {
	rdb, ctx := client()
	err := rdb.Set(ctx, key, value, 0).Err()
	if err != nil {
		return false
	}
	return true
}
