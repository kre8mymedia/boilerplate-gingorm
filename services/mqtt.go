package services

import (
	"os"
	"fmt"
    "sync"
    "math/rand"
	"time"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var HOST = os.Getenv("MQTT_HOST")
var PORT = os.Getenv("MQTT_PORT")
var USERNAME = os.Getenv("MQTT_USERNAME")
var MQTT_PASSWORD = os.Getenv("MQTT_PASSWORD")

var messagePubHandler mqtt.MessageHandler = func(_ mqtt.Client, msg mqtt.Message) {
    fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}

var connectHandler mqtt.OnConnectHandler = func(_ mqtt.Client) {
    fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(_ mqtt.Client, err error) {
    fmt.Printf("Connect lost: %v", err)
}

func fetchClient() mqtt.Client {
    opts := mqtt.NewClientOptions()
    opts.AddBroker(fmt.Sprintf("tcp://%s:%s", HOST, PORT))
    s := fmt.Sprintf("%v", rand.Float64())
    opts.SetClientID(s)
    opts.SetKeepAlive(2 * time.Second)
    opts.SetPingTimeout(1 * time.Second)
    opts.SetUsername(USERNAME)
    opts.SetPassword(MQTT_PASSWORD)
    opts.SetDefaultPublishHandler(messagePubHandler)
    opts.OnConnect = connectHandler
    opts.OnConnectionLost = connectLostHandler
    client := mqtt.NewClient(opts)
    if token := client.Connect(); token.Wait() && token.Error() != nil {
        panic(token.Error())
    }
	return client
}

func Publish(
    topic string,
    value string,
) (bool, error) {
    var client = fetchClient()
    fmt.Printf("%v %v\n", topic, value)
    token := client.Publish(topic, 1, false, value)
    
    token.WaitTimeout(1)
    // time.Sleep(time.Millisecond)
    return true, nil
}

func ThreadedPublish(
    count int, 
    topic string, 
    payload string,
) (bool, error) {
	var wg sync.WaitGroup
    for i := 0; i < count; i++ {
		// num := i
		wg.Add(1)
		go func() {
			defer wg.Done()
			phrase := payload
			// concatenated := fmt.Sprint(phrase, num)
			Publish(topic, phrase)
		}()
    }
	wg.Wait()
	return true, nil
}
