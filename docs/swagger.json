{
    "swagger": "2.0",
    "info": {
        "contact": {}
    },
    "basePath": "/api/v1",
    "paths": {
        "/books": {
            "get": {
                "description": "Returns a list of books",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Book"
                ],
                "summary": "List of books",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ListOfBooks"
                        }
                    }
                }
            },
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Book"
                ],
                "summary": "Adds a book to the list of books",
                "parameters": [
                    {
                        "description": "Book Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.CreateBookInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.SingleBookResponse"
                        }
                    }
                }
            }
        },
        "/books/{id}": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Book"
                ],
                "summary": "Find book",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Book Id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.SingleBookResponse"
                        }
                    }
                }
            },
            "delete": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Book"
                ],
                "summary": "Removes book from list of books",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Book Id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            },
            "patch": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Book"
                ],
                "summary": "Updates a book",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Book Id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "Book Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.UpdateBookInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.SingleBookResponse"
                        }
                    }
                }
            }
        },
        "/cache": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Redis"
                ],
                "summary": "GET Redis key by its key",
                "parameters": [
                    {
                        "description": "Redis Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controllers.CacheGetInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controllers.CacheResponse"
                        }
                    }
                }
            },
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Redis"
                ],
                "summary": "SET Redis key value",
                "parameters": [
                    {
                        "description": "Redis Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controllers.CacheInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controllers.CacheSetResponse"
                        }
                    }
                }
            }
        },
        "/organizations": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "Returns a list of organizations",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Organization"
                ],
                "summary": "List of organizations",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ListOfOrganizations"
                        }
                    }
                }
            },
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Organization"
                ],
                "summary": "Create organization and add primaryUser",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "description": "Book Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.CreateOrganizationInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.SingleOrganizationNoRelationsResponse"
                        }
                    }
                }
            }
        },
        "/organizations/{id}": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "Returns an organization by its Id",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Organization"
                ],
                "summary": "Find organization by its Id",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Authorization",
                        "name": "Authorization",
                        "in": "header",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Organization Id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.SingleOrganizationWithRelationsResponse"
                        }
                    }
                }
            }
        },
        "/pub": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "MQTT"
                ],
                "summary": "Publish message to topic",
                "parameters": [
                    {
                        "description": "Mqtt Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controllers.MqttInputNoCounter"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controllers.PublishResponse"
                        }
                    }
                }
            }
        },
        "/threaded/pub": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "MQTT"
                ],
                "summary": "Publish messages(n) to topic",
                "parameters": [
                    {
                        "description": "Mqtt Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controllers.MqttInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controllers.PublishResponse"
                        }
                    }
                }
            }
        },
        "/token": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Auth"
                ],
                "summary": "Retrieve Authentication Token",
                "parameters": [
                    {
                        "description": "Auth Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controllers.TokenRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controllers.Token"
                        }
                    }
                }
            }
        },
        "/user/register": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Auth"
                ],
                "summary": "Register user for application",
                "parameters": [
                    {
                        "description": "Auth Data",
                        "name": "message",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.CreateUserInput"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.UserNoPasswordOrName"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "controllers.CacheGetInput": {
            "type": "object",
            "required": [
                "key"
            ],
            "properties": {
                "key": {
                    "type": "string"
                }
            }
        },
        "controllers.CacheInput": {
            "type": "object",
            "required": [
                "key"
            ],
            "properties": {
                "key": {
                    "type": "string"
                },
                "value": {}
            }
        },
        "controllers.CacheResponse": {
            "type": "object",
            "properties": {
                "success": {
                    "type": "boolean"
                },
                "value": {}
            }
        },
        "controllers.CacheSetResponse": {
            "type": "object",
            "properties": {
                "success": {
                    "type": "boolean"
                }
            }
        },
        "controllers.MqttInput": {
            "type": "object",
            "required": [
                "payload",
                "topic"
            ],
            "properties": {
                "count": {
                    "type": "integer"
                },
                "payload": {
                    "type": "string"
                },
                "topic": {
                    "type": "string"
                }
            }
        },
        "controllers.MqttInputNoCounter": {
            "type": "object",
            "required": [
                "payload",
                "topic"
            ],
            "properties": {
                "payload": {
                    "type": "string"
                },
                "topic": {
                    "type": "string"
                }
            }
        },
        "controllers.PublishResponse": {
            "type": "object",
            "properties": {
                "published": {
                    "type": "boolean"
                }
            }
        },
        "controllers.Token": {
            "type": "object",
            "properties": {
                "token": {
                    "type": "string"
                }
            }
        },
        "controllers.TokenRequest": {
            "type": "object",
            "properties": {
                "email": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        },
        "models.Book": {
            "type": "object",
            "properties": {
                "author": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "models.CreateBookInput": {
            "type": "object",
            "required": [
                "author",
                "title"
            ],
            "properties": {
                "author": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "models.CreateOrganizationInput": {
            "type": "object",
            "required": [
                "name",
                "slug"
            ],
            "properties": {
                "name": {
                    "type": "string"
                },
                "slug": {
                    "type": "string"
                }
            }
        },
        "models.CreateUserInput": {
            "type": "object",
            "required": [
                "email",
                "name",
                "password",
                "username"
            ],
            "properties": {
                "email": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "models.ListOfBooks": {
            "type": "object",
            "properties": {
                "books": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.Book"
                    }
                }
            }
        },
        "models.ListOfOrganizations": {
            "type": "object",
            "properties": {
                "organizations": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.OrganizationNoRelations"
                    }
                }
            }
        },
        "models.OrganizationNoRelations": {
            "type": "object",
            "properties": {
                "createdAt": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "primaryUserId": {
                    "type": "string"
                },
                "slug": {
                    "type": "string"
                }
            }
        },
        "models.OrganizationWithRelations": {
            "type": "object",
            "properties": {
                "createdAt": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "primaryUserId": {
                    "type": "string"
                },
                "slug": {
                    "type": "string"
                },
                "users": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.UserNoPassword"
                    }
                }
            }
        },
        "models.SingleBookResponse": {
            "type": "object",
            "properties": {
                "book": {
                    "$ref": "#/definitions/models.Book"
                }
            }
        },
        "models.SingleOrganizationNoRelationsResponse": {
            "type": "object",
            "properties": {
                "organization": {
                    "$ref": "#/definitions/models.OrganizationNoRelations"
                },
                "success": {
                    "type": "boolean"
                }
            }
        },
        "models.SingleOrganizationWithRelationsResponse": {
            "type": "object",
            "properties": {
                "organization": {
                    "$ref": "#/definitions/models.OrganizationWithRelations"
                },
                "success": {
                    "type": "boolean"
                }
            }
        },
        "models.UpdateBookInput": {
            "type": "object",
            "properties": {
                "author": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "models.UserNoPassword": {
            "type": "object",
            "required": [
                "email",
                "name",
                "username"
            ],
            "properties": {
                "email": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "models.UserNoPasswordOrName": {
            "type": "object",
            "required": [
                "email",
                "username"
            ],
            "properties": {
                "email": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}