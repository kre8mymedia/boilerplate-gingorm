module github.com/kre8mymedia/gingorm

go 1.16

require (
	github.com/aidarkhanov/nanoid v1.0.8
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/gin-gonic/gin v1.7.7
	github.com/go-openapi/spec v0.20.6 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/go-playground/validator/v10 v10.11.0 // indirect
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2
	github.com/swaggo/gin-swagger v1.4.3
	github.com/swaggo/swag v1.8.4
	github.com/swaggo/swag/example/celler v0.0.0-20220719092602-cc25410f3556
	github.com/ugorji/go v1.2.7 // indirect
	github.com/urfave/cli/v2 v2.11.1 // indirect
	golang.org/x/crypto v0.0.0-20220507011949-2cf3adece122
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/tools v0.1.11 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
