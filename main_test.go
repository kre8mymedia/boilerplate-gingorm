package main

import (
	"log"
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/kre8mymedia/gingorm/models"
)

func init() {
	// Set Test Env
	os.Setenv("DB_HOST", "127.0.0.1")
	os.Setenv("DB_PORT", "3306")
	os.Setenv("DB_DATABASE", "gingorm")
	os.Setenv("DB_USERNAME", "gingorm")
	os.Setenv("DB_PASSWORD", "test1234")
	// Initialize DB
	models.ConnectDatabase()
	gin.SetMode(gin.TestMode)
}

var DB *gorm.DB

func clearDatabase() {
	DB.DropTable(&models.Book{})
}

const ID string = "1"

func mockRequest(
	method string,
	url string,
	body []byte,
) (
	r *gin.Engine,
	req *http.Request,
	w *httptest.ResponseRecorder,
) {
	
	// Intialize Server
	r = initRouter()
	// Construct Request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
    if err != nil {
        log.Printf("Couldn't create request: %v\n", err)
    }
	// Form response
	w = httptest.NewRecorder()
	return r, req, w
}

func TestHomePage(t *testing.T) {
	r, req, w := mockRequest("GET", "/", nil)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }
}	

func TestFindBooks(t *testing.T) {
	r, req, w := mockRequest("GET", "/api/v1/books", nil)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }
}

func TestCreateBook(t *testing.T) {

	var jsonData = []byte(`{
		"title": "morpheus",
		"author": "Ryan Eggz"
	}`)

	r, req, w := mockRequest("POST", "/api/v1/books", jsonData)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }
}

func TestFindBook(t *testing.T) {
	r, req, w := mockRequest("GET", "/api/v1/books/" + ID, nil)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }
}

func TestUpdateBook(t *testing.T) {
	var jsonData = []byte(`{
		"title": "Capernicus",
		"author": "Ryan Eggz"
	}`)

	r, req, w := mockRequest("PATCH", "/api/v1/books/" + ID, jsonData)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }
}

func TestDeleteBook(t *testing.T) {

	r, req, w := mockRequest("DELETE", "/api/v1/books/" + ID, nil)
	r.ServeHTTP(w, req)
    // fmt.Println(w.Body)
	if w.Code == http.StatusOK {
        t.Logf("Expected to get status %d is same ast %d\n", http.StatusOK, w.Code)
    } else {
        t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
    }

	t.Cleanup(func(){
        //tear-down code
    })
}
