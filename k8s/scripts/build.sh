#!/bin/bash

## Check if TAG was passed
if [ -z "$1" ]
then
      echo "ERROR: First argument for TAG is empty"
      exit
fi

## Go to root of project
cd $(dirname $0)
cd ../../
DIR=$(pwd)

## Build and push image with input tag
docker build -t "sk8er71091/gingorm:$1" .
docker push "sk8er71091/gingorm:$1"

## Print details
echo ""
echo ""
echo "----------------------------------------------------"
echo ">> Version: $1"
echo ">> Image: sk8er71091/gingorm:$1"
echo "----------------------------------------------------"
