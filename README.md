# Golang Gin Gorm Boilerplate Project

## Table of Contents
- [HTTP Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

### Build Docker Network (Gin & Gorm, MySQL, PHPMyAdmin, Redis, RedisUi, MQTT)
```bash
docker-compose up --build
```

### Run Node-RED
```bash
bash scripts/local_node_red.sh
```

### Run Tests
Unit Tests
```bash
bash scripts/test_unit.sh
```

Integration Tests
```bash
bash scripts/test.sh
```

### Build Docker Image for Kubernetes
Will build Golang Server Image and push to dockerhub.
```bash
bash k8s/scripts/build.sh $TAG ## Replace tag with the image docker tag
```

### Upgrade Golang Kubernetes Pods
Will create pods or update existing pods
```bash
bash k8s/scripts/upgrade.sh $APP_ENV $NAMESPACE $TAG  ## Replace tag with the image docker tag
```