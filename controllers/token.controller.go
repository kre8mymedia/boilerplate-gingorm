package controllers
import (
	"github.com/kre8mymedia/gingorm/auth"
	"github.com/kre8mymedia/gingorm/models"
	"net/http"
	"github.com/gin-gonic/gin"
)
type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Token struct {
	Token    string `json:"token"`
}

// AuthToken example
// @Summary Retrieve Authentication Token
// @Tags Auth
// @Accept  json
// @Produce  json
// @Param message body TokenRequest true "Auth Data"
// @Success 200 {object} Token
// @Router /token [post]
func GenerateToken(context *gin.Context) {
	var request TokenRequest
	var user models.User
	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	// check if email exists and password is correct
	record := models.DB.Where("email = ?", request.Email).First(&user)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	credentialError := user.CheckPassword(request.Password)
	if credentialError != nil {
		context.JSON(http.StatusUnauthorized, gin.H{"error": "invalid credentials"})
		context.Abort()
		return
	}
	tokenString, err:= auth.GenerateJWT(user.Email, user.Username, user.ID)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, gin.H{"token": tokenString})
}