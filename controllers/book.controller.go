package controllers

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/kre8mymedia/gingorm/models"
)



// PingExample godoc
// @Summary List of books
// @Schemes
// @Description Returns a list of books
// @Tags Book
// @Accept json
// @Produce json
// @Success 200 {object} models.ListOfBooks
// @Router /books [get]
func FindBooks(c *gin.Context) {
  var books []models.Book
  models.DB.Find(&books)

  c.JSON(http.StatusOK, gin.H{"books": books})
}

// AddBook example
// @Summary Adds a book to the list of books
// @Tags Book
// @Accept  json
// @Produce  json
// @Param message body models.CreateBookInput true "Book Data"
// @Success 200 {object} models.SingleBookResponse
// @Router /books [post]
func CreateBook(c *gin.Context) {
	// Validate input
	var input models.CreateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	// Create book
	book := models.Book{Title: input.Title, Author: input.Author}
	models.DB.Create(&book)
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// FindBook example
// @Summary Find book
// @Tags Book
// @Accept  json
// @Produce  json
// @Param id path string true "Book Id"
// @Success 200 {object} models.SingleBookResponse
// @Router /books/{id} [get]
func FindBook(c *gin.Context) {  // Get model if exist
	var book models.Book
  
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// UpdateBook example
// @Summary Updates a book
// @Tags Book
// @Accept  json
// @Produce  json
// @Param id path string true "Book Id"
// @Param message body models.UpdateBookInput true "Book Data"
// @Success 200 {object} models.SingleBookResponse
// @Router /books/{id} [patch]
func UpdateBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	// Validate input
	var input models.UpdateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	models.DB.Model(&book).Updates(input)
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// DeleteBook example
// @Summary Removes book from list of books
// @Tags Book
// @Accept  json
// @Produce  json
// @Param id path string true "Book Id"
// @Success 200 {string} string	"ok"
// @Router /books/{id} [delete]
func DeleteBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	models.DB.Delete(&book)
  
	c.JSON(http.StatusOK, gin.H{"success": true})
}
