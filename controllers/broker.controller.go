package controllers

import (
	"net/http"
  
	"github.com/gin-gonic/gin"
	services "github.com/kre8mymedia/gingorm/services"
)

type MqttInput struct {
	Topic   string `json:"topic" binding:"required"`
	Payload string `json:"payload" binding:"required"`
	Count   int    `json:"count"`
}

type MqttInputNoCounter struct {
	Topic   string `json:"topic" binding:"required"`
	Payload string `json:"payload" binding:"required"`
}

type PublishResponse struct {
	Published bool `json:"published"`
}

// MQTT Publish
// @Summary Publish message to topic
// @Tags MQTT
// @Accept  json
// @Produce  json
// @Param message body MqttInputNoCounter true "Mqtt Data"
// @Success 200 {object} PublishResponse
// @Router /pub [post]
func BrokerPublish(ctx *gin.Context) {
	// request
	var input MqttInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// action
	payload, err := services.Publish(input.Topic, input.Payload)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"published": false})
	}
	// response
	ctx.JSON(http.StatusOK, gin.H{"published": payload})
}

// MQTT Threaded Publish
// @Summary Publish messages(n) to topic
// @Tags MQTT
// @Accept  json
// @Produce  json
// @Param message body MqttInput true "Mqtt Data"
// @Success 200 {object} PublishResponse
// @Router /threaded/pub [post]
func BrokerThreadedPublish(ctx *gin.Context) {
	// request
	var input MqttInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// action
	payload, err := services.ThreadedPublish(input.Count, input.Topic, input.Payload)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"published": false})
	}
	// response
	ctx.JSON(http.StatusOK, gin.H{"published": payload})
}