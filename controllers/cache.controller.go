package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/kre8mymedia/gingorm/services"
)

type CacheInput struct {
	Key   string      `json:"key" binding:"required"`
	Value interface{} `json:"value"`
}

type CacheGetInput struct {
	Key string `json:"key" binding:"required"`
}

type CacheResponse struct {
	Success bool        `json:"success"`
	Value   interface{} `json:"value"`
}

type CacheSetResponse struct {
	Success bool `json:"success"`
}

// Redis GET
// @Summary GET Redis key by its key
// @Tags Redis
// @Accept  json
// @Produce  json
// @Param message body CacheGetInput true "Redis Data"
// @Success 200 {object} CacheResponse
// @Router /cache [get]
func CacheGet(ctx *gin.Context) {
	var input CacheInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	payload, err := services.RedisGet(input.Key)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"success": false})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"success": true,
		"value":   payload,
	})
}

// Redis SET
// @Summary SET Redis key value
// @Tags Redis
// @Accept  json
// @Produce  json
// @Param message body CacheInput true "Redis Data"
// @Success 200 {object} CacheSetResponse
// @Router /cache [post]
func CacheSet(ctx *gin.Context) {
	var input CacheInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	payload := services.RedisSet(input.Key, input.Value)
	if payload != true {
		ctx.JSON(http.StatusOK, gin.H{"success": false})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"success": payload})
}
