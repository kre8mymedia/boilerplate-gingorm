package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/kre8mymedia/gingorm/models"
	orgRepo "github.com/kre8mymedia/gingorm/repositories/organizations"
	userRepo "github.com/kre8mymedia/gingorm/repositories/users"

	"github.com/jinzhu/gorm"
)

// ListOrganizations godoc
// @Summary List of organizations
// @Schemes
// @Description Returns a list of organizations
// @Tags Organization
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Success 200 {object} models.ListOfOrganizations
// @Router /organizations [get]
func FindOrganizations(c *gin.Context) {
	organizations := orgRepo.All()

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"organizations": organizations,
	})
}

// CreateOrganization
// @Summary Create organization and add primaryUser
// @Tags Organization
// @Accept  json
// @Produce  json
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Param message body models.CreateOrganizationInput true "Book Data"
// @Success 200 {object} models.SingleOrganizationNoRelationsResponse
// @Router /organizations [post]
func CreateOrganization(c *gin.Context) {
	// Retrieve User
	user := userRepo.AuthUser(c.Request.Header["Authorization"][0])

	// Check authorized user
	if user.OrganizationID != c.Param("id") {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	// Validate input
	var input models.CreateOrganizationInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	// Create organization
	organization := models.Organization{
		PrimaryUserID: user.ID,
		Name: input.Name,
		Slug: input.Slug,
	}

	if err := models.DB.Create(&organization).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Duplicate Entry"})
	  	return
	}

	models.DB.Model(&user).Update("OrganizationID", organization.ID)
  
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"organization": organization,
	})
}

// FindOrganizaton example
// @Summary Find organization by its Id
// @Schemes
// @Description Returns an organization by its Id
// @Tags Organization
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Param id path string true "Organization Id"
// @Success 200 {object} models.SingleOrganizationWithRelationsResponse
// @Router /organizations/{id} [get]
func FindOrganization(c *gin.Context) {  // Get model if exist
	// Retrieve User
	user := userRepo.AuthUser(c.Request.Header["Authorization"][0])

	// Check authorized user
	if user.OrganizationID != c.Param("id") {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	// Retrieve Organization
	var organization models.Organization
	if err := models.DB.Preload("Users", func(db *gorm.DB) *gorm.DB {
		return db.Select([]string{"id", "name", "email", "username"})
	}).Where("id = ?", c.Param("id")).First(&organization).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"organization": organization,
	})
}