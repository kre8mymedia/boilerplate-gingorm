package entities

var Topic = map[string]string{
	"device": "write/devices",
	"test" : "write/tests",
	"event" : "write/events",
	"meeting" : "write/meetings",
	"channel" : "write/channels",
}